import java.util.ArrayList;

public class ArraysExercise1 {

    public static void main(String[] args) {

        ArrayList<Double> myDouble = new ArrayList<>();

        for (double dbl = 0.0; dbl <= 10.0; dbl += 0.5) {
            myDouble.add(dbl);
        }

        for (int i = 0; i < myDouble.size(); i++) {
            double value = myDouble.get(i);
            System.out.println(i + " -->" + value);
        }

    }


}