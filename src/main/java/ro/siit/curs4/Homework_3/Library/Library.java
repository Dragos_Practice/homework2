package ro.siit.curs4.Homework_3.Library;


public class Library {

    public static void main(String[] args) {
        Author author1 = new Author("Mihai Eminescu", "mihai.eminescu@gmail.com");
        Book book1 = new Book("Poezii", 1883, author1, 57.8);


        System.out.println("Book " + book1.getName() + " " + "(" + book1.getPrice() + " " + "RON" + ")" + "," + " by " + author1.getName() + "," + " published " + "in " + book1.getYear() + "." );
    }


}

