import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.config.DriverManagerType;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;


public class Exercise {


    @DataProvider(name="registerdp")
    public Iterator<Object[]> registerDp() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        //firstName, lastName, email, userName, password, confirmPassword, firstNameErrorMessage,
        //lastNameErrorMessage, emailErrorMessage, userNameErrorMessage, passwordErrorMessage, confirmPassWordErrorMessage
        dp.add(new String[] {"", "", "", "", "", "", "Invalid input. Please enter between 2 and 35 letters", "Invalid input. Please enter between 2 and 35 letters", "Invalid input. Please enter a valid email address", "Invalid input. Please enter between 4 and 35 letters or numbers", "Invalid input. Please use a minimum of 8 characters", "Please confirm your password"});
        dp.add(new String[] {"", "Smith", "test@test.com", "Johnny", "111111111", "111111111", "Invalid input. Please enter between 2 and 35 letters", "", "", "", "", ""});
        dp.add(new String[] {"John", "", "test@test.com", "Johnny", "111111111", "111111111", "", "Invalid input. Please enter between 2 and 35 letters", "", "", "", ""});
        dp.add(new String[] {"John", "Smith", "", "Johnny", "111111111", "111111111", "", "", "Invalid input. Please enter a valid email address", "", "", ""});
        dp.add(new String[] {"John", "Smith", "test@test.com", "", "111111111", "111111111", "", "", "", "Invalid input. Please enter between 4 and 35 letters or numbers", "", ""});
        dp.add(new String[] {"John", "Smith", "test@test.com", "Johnny", "", "111111111", "", "", "", "", "Invalid input. Please use a minimum of 8 characters", "Please confirm your password"});
        dp.add(new String[] {"John", "Smith", "test@test.com", "Johnny", "111111111", "", "", "", "", "", "", "Please confirm your password"});

        //Boundary + Numbers + Special chars pentru FirstName
        dp.add(new String[] {"J", "Smith", "test@test.com", "Johnny", "111111111", "111111111", "Invalid input. Please enter between 2 and 35 letters", "", "", "", "", ""});
        dp.add(new String[] {"JohnJohnJohnJohnJohnJohnJohnJohnJohnJohn", "Smith", "test@test.com", "Johnny", "111111111", "111111111", "Invalid input. Please enter between 2 and 35 letters", "", "", "", "", ""});
        dp.add(new String[] {"12345", "Smith", "test@test.com", "Johnny", "111111111", "111111111", "Invalid input. Please enter between 2 and 35 letters", "", "", "", "", ""});
        dp.add(new String[] {"#$%^&", "Smith", "test@test.com", "Johnny", "111111111", "111111111", "Invalid input. Please enter between 2 and 35 letters", "", "", "", "", ""});

        //Boundary + Numbers pentru LastName
        dp.add(new String[] {"John", "S", "test@test.com", "Johnny", "111111111", "111111111", "", "Invalid input. Please enter between 2 and 35 letters", "", "", "", ""});
        dp.add(new String[] {"John", "SmithSmithSmithSmithSmithSmithSmith", "test@test.com", "Johnny", "111111111", "111111111", "", "Invalid input. Please enter between 2 and 35 letters", "", "", "", ""});
        dp.add(new String[] {"John", "12345", "test@test.com", "Johnny", "111111111", "111111111", "", "Invalid input. Please enter between 2 and 35 letters", "", "", "", ""});

        //Fara @ la email + 2 x @
        dp.add(new String[] {"John", "Smith", "testtestcom", "Johnny", "111111111", "111111111", "", "", "Invalid input. Please enter a valid email address", "", "", ""});
        dp.add(new String[] {"John", "Smith", "test@test@com", "Johnny", "111111111", "111111111", "", "", "Invalid input. Please enter a valid email address", "", "", ""});

        //Boundary + Special chars pentru Username
        dp.add(new String[] {"John", "Smith", "test@test.com", "Jon", "111111111", "111111111", "", "", "", "Invalid input. Please enter between 4 and 35 letters or numbers", "", ""});
        dp.add(new String[] {"John", "Smith", "test@test.com", "JohnnyJohnnyJohnnyJohnnyJohnnyJohnnyJohnnyJohnnyJohnnyJohnnyJohnnyJohnny", "111111111", "111111111", "", "", "", "Invalid input. Please enter between 4 and 35 letters or numbers", "", ""});
        dp.add(new String[] {"John", "Smith", "test@test.com", "@#$%^", "111111111", "111111111", "", "", "", "Invalid input. Please enter between 4 and 35 letters or numbers", "", ""});


        return dp.iterator();

    }

    @Test(dataProvider = "registerdp")
    public void negativeRegisterTest(String firstName, String lastName, String email, String userName, String password, String confirmPassword, String firstNameErrorMessage, String lastNameErrorMessage, String emailErrorMessage, String usernameErrorMessage, String passwordErrorMessage, String confirmPasswordErrorMessage) {
        WebDriverManager.getInstance(DriverManagerType.CHROME).setup();
        WebDriver driver = new ChromeDriver();
        driver.get("http://86.121.249.149:4999/stubs/auth.html#registration_panel");
        WebElement firstNameInput = driver.findElement(By.id("inputFirstName"));
        WebElement lastNameInput = driver.findElement(By.id("inputLastName"));
        WebElement emailInput = driver.findElement(By.id("inputEmail"));
        WebElement usernameInput = driver.findElement(By.id("inputUsername"));
        WebElement passwordInput = driver.findElement(By.id("inputPassword"));
        WebElement confirmPasswordInput = driver.findElement(By.id("inputPassword2"));
        WebElement submitButton = driver.findElement(By.id("register-submit"));
        WebElement registerTab = driver.findElement(By.id("register-tab"));



        registerTab.click();
        firstNameInput.clear();
        firstNameInput.sendKeys(firstName);
        lastNameInput.clear();
        lastNameInput.sendKeys(lastName);
        emailInput.clear();
        emailInput.sendKeys(email);
        usernameInput.clear();
        usernameInput.sendKeys(userName);
        passwordInput.clear();
        passwordInput.sendKeys(password);
        confirmPasswordInput.clear();
        confirmPasswordInput.sendKeys(confirmPassword);
        submitButton.submit();



        WebElement err1 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[3]/div/div[2]"));
        WebElement err2 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[4]/div/div[2]"));
        WebElement err3 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[5]/div[1]/div[2]"));
        WebElement err4 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[6]/div/div[2]"));
        WebElement err5 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[7]/div/div[2]"));
        WebElement err6 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[8]/div/div[2]"));


        Assert.assertEquals(err1.getText(), firstNameErrorMessage);
        Assert.assertEquals(err2.getText(), lastNameErrorMessage);
        Assert.assertEquals(err3.getText(), emailErrorMessage);
        Assert.assertEquals(err4.getText(), usernameErrorMessage);
        Assert.assertEquals(err5.getText(), passwordErrorMessage);
        Assert.assertEquals(err6.getText(), confirmPasswordErrorMessage);

        driver.close();


    }




}
